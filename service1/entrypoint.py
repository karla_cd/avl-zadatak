import hashlib
from flask import Flask, request
app = Flask(__name__)

@app.route('/', methods=['POST'])
def result():
    print(request.form)

    hash_func = request.form["digest_alg"].strip()
    message = request.form["message"].strip()

    h = hashlib.new(hash_func)
    h.update(str.encode(message))

    print(h.hexdigest())
    return h.hexdigest()

if __name__ == '__main__':
    app.run()
