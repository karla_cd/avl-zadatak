import requests
import os

SERVICE1_URL = os.environ["SERVICE1_URL"]

message = requests.get('https://www.w3schools.com/python/demopage.htm').text

data ={"digest_alg": "md5", "message": message}

print(data)
print(requests.post(SERVICE1_URL, data=data).text)
