import os, argparse, dotenv

# python deploy.py --version_service1 v1.0 --version_service2 v1.0
# python deploy.py

if __name__ == "__main__":

    dotenv_file = dotenv.find_dotenv()
    dotenv.load_dotenv(dotenv_file)

    parser = argparse.ArgumentParser()
    parser.add_argument("--version_service1")
    parser.add_argument("--version_service2")

    args = parser.parse_args()

    if args.version_service1 is not None:
        dotenv.set_key(dotenv_file, "SERVICE1_VERSION", args.version_service1)

    if args.version_service2 is not None:
        dotenv.set_key(dotenv_file, "SERVICE2_VERSION", args.version_service2)

    os.system("docker compose up --build")
